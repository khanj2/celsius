package sheridan;

/**
 * 
 * @author Jawad Khan | 991556484
 *
 */

public class Celsius {
	
	/**
	 * 
	 * @param temp
	 * @return temperature in celsius after converting from fahrenheit
	 * 
	 * Refactor history:
	 * Started with simple return 0
	 * 
	 */
	public static int fromFahrenheit (int temp) {
		return (int) Math.ceil( ((temp - 32) / 1.8) );
	}
	
}

package sheridan;

import static org.junit.Assert.*;

// Jawad Khan | 991556484

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int expected = 5;
		assertTrue("The value is not equal", Celsius.fromFahrenheit(41) == expected);
	}


	@Test 
	public void testFromFahrenheitException() {
		int expected = -5;
		assertTrue("The value is not equal", Celsius.fromFahrenheit(23) == expected);
		
	}
	
	 @Test public void testFromFahrenheitBoundaryIn() {
		 int expected = 2;
		 assertTrue("The value is not equal", Celsius.fromFahrenheit(35) == expected);
	 }
	  
	 @Test public void testFromFahrenheitBoundaryOut() {
		 int expected = 1;
		 assertFalse("The value is not equal", Celsius.fromFahrenheit(35) == expected);
	 }

}
